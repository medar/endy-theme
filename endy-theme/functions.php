<?php
/**
 * endy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package endy
 */

if ( ! function_exists( 'endy_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function endy_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on endy, use a find and replace
		 * to change 'endy' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'endy', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'blog-thumb', 725, 400, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'endy-theme' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'endy-theme' ),
		) );

		register_nav_menus( array(
			'social' => esc_html__( 'Social', 'endy-theme' ),
		) );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'endy_custom_background_args', array(
			'default-color' => 'f6f6f6',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'endy_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function endy_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'endy_content_width', 640 );
}

add_action( 'after_setup_theme', 'endy_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function endy_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'endy' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'endy' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Blog posts sidebar', 'endy' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'endy' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'endy_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function endy_scripts() {

	$theme_style =  fw_get_db_settings_option('theme_style');
	$theme_style .= '/' . $theme_style . '.css';

	wp_enqueue_style(
		'bootstrap-style',
		get_template_directory_uri() . '/assets/css/libs/bootstrap.min.css'
	);

	wp_enqueue_style(
		'hamburgers-style',
		get_template_directory_uri() . '/assets/css/libs/hamburgers.min.css'
	);

	wp_enqueue_style(
		'lg-style',
		get_template_directory_uri() . '/libs/lightgallery/css/lightgallery.css'
	);

	wp_enqueue_style(
		'endy-style',
		get_stylesheet_uri()
	);

	wp_enqueue_style(
	  'theme-style',
      get_template_directory_uri() . '/assets/css/theme-style/' . $theme_style
    );

	wp_enqueue_script(
		'endy-navigation',
		get_template_directory_uri() . '/js/navigation.js',
		array(),
		'20151215',
		true
	);

	wp_enqueue_script(
		'endy-skip-link-focus-fix',
		get_template_directory_uri() . '/js/skip-link-focus-fix.js',
		array(),
		'20151215',
		true
	);

	wp_enqueue_script(
		'isotope-js',
		get_template_directory_uri() . '/js/isotope.pkgd.min.js',
		array( 'jquery' ),
		'3.0.3',
		true
	);

	wp_enqueue_script(
		'isotope-packery-js',
		get_template_directory_uri() . '/js/packery-mode.pkgd.min.js',
		array( 'isotope-js' ),
		'2.0.0',
		true
	);

	wp_enqueue_script(
		'imagesloaded-js',
		get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js',
		array( 'jquery' ),
		'4.1.1',
		true
	);

	wp_enqueue_script(
		'lightgallery-js',
		get_template_directory_uri() . '/libs/lightgallery/js/lightgallery.min.js',
		array( 'jquery' ),
		'1.3.9',
		true
	);

	wp_enqueue_script(
		'lg-thumbnail-js',
		get_template_directory_uri() . '/libs/lg-thumbnail/lg-thumbnail.min.js',
		array( 'lightgallery-js' ),
		'1.0.3',
		true
	);

	wp_enqueue_script(
		'lg-zoom-js',
		get_template_directory_uri() . '/libs/lg-zoom/lg-zoom.min.js',
		array( 'lightgallery-js' ),
		'1.0.4',
		true
	);

	wp_enqueue_script(
		'theme-js',
		get_template_directory_uri() . '/js/theme.js',
		array( 'jquery', 'masonry' ),
		'1.0.0',
		true
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'endy_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

require get_template_directory() . '/tgm/endy.php';

require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom Post Types
 */
require get_template_directory() . '/inc/post-types/CPT.php';

/**
 * Portfolio Custom Post Type
 */
require get_template_directory() . '/inc/post-types/register-portfolio.php';

/**
 * Load shortcodes
 */
require get_template_directory() . '/inc/shortcode.php';

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 *
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
	return 20;
}

add_filter( 'excerpt_more', function ( $more ) {
	return '';
} );
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

if ( ! function_exists( 'endy_comment_form' ) ) :
	/**
	 * Outputs a complete commenting form for use within a template.
	 * Most strings and form fields may be controlled through the $args array passed
	 * into the function, while you may also choose to use the comment_form_default_fields
	 * filter to modify the array of default fields if you'd just like to add a new
	 * one or remove a single field. All fields are also individually passed through
	 * a filter of the form comment_form_field_$name where $name is the key used
	 * in the array of fields.
	 *
	 * @param array $args Options for strings, fields etc in the form
	 * @param mixed $post_id Post ID to generate the form for, uses the current post if null
	 *
	 * @return void
	 */
	function endy_comment_form( $args = array(), $post_id = null ) {
		global $id;

		$user          = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : '';

		if ( null === $post_id ) {
			$post_id = $id;
		} else {
			$id = $post_id;
		}

		if ( comments_open( $post_id ) ) :
			?>
            <div id="respond-wrap">
				<?php
				$commenter = wp_get_current_commenter();
				$req       = get_option( 'require_name_email' );
				$aria_req  = ( $req ? " aria-required='true'" : '' );
				$fields    = array(
					'author' => '<div class="row"><p class="comment-form-author col-sm-6">'
					            //                                . '<label for="author">' . __( 'Name', 'endy' ) . '</label> '
					            //                                . ( $req ? '<span class="required">*</span>' : '' )
					            . '<input id="author" name="author" type="text" class="form-control" value="'
					            . esc_attr( $commenter['comment_author'] )
					            . '" size="30"' . $aria_req
					            . ' placeholder="' . __( 'Name', 'endy' ) . ' *' . '" /></p>',
					'email'  => '<p class="comment-form-email col-sm-6">'
					            //					            . '<label for="email">' . __( 'Email', 'endy' ) . '</label> '
					            //					            . ( $req ? '<span class="required">*</span>' : '' )
					            . '<input id="email" name="email" type="text" class="form-control" value="'
					            . esc_attr( $commenter['comment_author_email'] ) .
					            '" size="30"' . $aria_req
					            . ' placeholder="' . __( 'Email', 'endy' ) . ' *'
					            . '" /></p></div>'
				);

				if ( function_exists( 'bp_is_active' ) ) {
					$profile_link = bp_get_loggedin_user_link();
				} else {
					$profile_link = admin_url( 'profile.php' );
				}

				$comments_args = array(
					'fields'            => apply_filters( 'comment_form_default_fields', $fields ),
					'logged_in_as'      => '<p class="logged-in-as">' . sprintf( __( 'Вы вошли как <a href="%1$s">%2$s</a>. <a href="%3$s" title="Выйти из этой учетной записи">Выйти?</a>', 'endy' ), $profile_link, $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) ) . '</p>',
					'title_reply'       => __( 'Добавить комментарий', 'endy' ),
					'title_reply_to'    => __( 'Добавить комментарий в %s', 'endy' ),
					'cancel_reply_link' => __( 'Нажмите здесь, чтобы отменить ответ', 'endy' ),
					'label_submit'      => __( 'Send message', 'endy' ),
					'comment_field'     => '<p class="comment-form-comment">'
					                       //                                           .'<label for="comment">' . __( 'Your message', 'endy' ) . '</label>'
					                       . '<textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"'
					                       . 'placeholder="' . __( 'Your message', 'endy' ) . '" ></textarea>'
					                       . '</p>',
					'must_log_in'       => '<p class="must-log-in">' . sprintf( __( 'Вы должны быть  <a href="%s">авторизированы</a> для добавления комментария.', 'endy' ), wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) ) . '</p>',
				);

				comment_form( $comments_args );
				?>
            </div>

			<?php
		endif;

	}
endif;

class Endy_Social_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'endy_social_widget',
			'Endy Social Widget',
			array( 'description' => __( 'A Social Widget', 'endy' ), )
		);
	}

	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo '<section class="widget endy-social-widget">';
		if ( ! empty( $title ) ) {
			echo '<h2>' . $title . '</h2>';
		}

		?>
		<?php if ( has_nav_menu( 'social' ) ) : // Check if there's a menu assigned to the 'social' location. ?>

            <nav class="menu-social social-navigation menu" role="navigation"
                 aria-label="<?php esc_attr_e( 'Social Menu', 'some' ); ?>">

				<?php wp_nav_menu(
					array(
						'theme_location'  => 'social',
						'container_class' => 'social-menu-wrapper',
//						'menu_id'         => 'menu-social-items',
						'menu_class'      => 'menu-social-items',
						'depth'           => 1,
						'link_before'     => '<span class="screen-reader-text">',
						'link_after'      => '</span>' . endy_get_svg( array( 'icon' => 'rating-full' ) ),
						'fallback_cb'     => '',
					)
				); ?>
            </nav><!-- .menu-social -->

		<?php endif; ?>
		<?php
		echo '</section>';
	}

	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'New title', 'endy' );
		}
		?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"/>
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

}

add_action( 'widgets_init', function () {
	register_widget( 'Endy_Social_Widget' );
} );

/* Tagcloud, change the font size */
function custom_tag_cloud_widget( $args ) {
	$args['largest']  = 14; //largest tag
	$args['smallest'] = 14; //smallest tag
	$args['unit']     = 'px'; //tag font unit

	return $args;
}

add_filter( 'widget_tag_cloud_args', 'custom_tag_cloud_widget' );