<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package endy
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( "col-xs-12 col-sm-6 col-md-4 masonry-grid-item" ); ?>>
    <header class="entry-header">
        <a href="<?php echo esc_url( get_permalink() ); ?>" class="wrap-blog-thumb" rel="bookmark"><?php
		    the_post_thumbnail( 'blog-thumb', array( 'class' => 'blog-preview' ) );
		    ?></a>
        <?php
		if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
				<?php endy_posted_on(); ?>
            </div><!-- .entry-meta -->
			<?php
		endif;
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

 ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
		<?php
		//			the_content( sprintf(
		//				/* translators: %s: Name of current post. */
		//				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'endy' ), array( 'span' => array( 'class' => array() ) ) ),
		//				the_title( '<span class="screen-reader-text">"', '"</span>', false )
		//			) );
		the_excerpt();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'endy' ),
			'after'  => '</div>',
		) );
		?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
<!--		--><?php //endy_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
