<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package endy
 */


?>

<div class="row">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container-fluid">
            <div class="row">
                <header class="entry-header col-xs-12">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;
					echo '<figure>';
					the_post_thumbnail( 'full' );
					echo '</figure>';

					if ( 'post' === get_post_type() ) : ?>
                        <div class="entry-meta">
                            <!--				--><?php //endy_posted_on(); ?>
                        </div><!-- .entry-meta -->
						<?php
					endif; ?>
                </header><!-- .entry-header -->
            </div><!-- .row -->

            <div class="row">
                <div class="entry-content col-sm-8">
					<?php
					the_content( sprintf(
					/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'endy' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'endy' ),
						'after'  => '</div>',
					) );
					if ( comments_open() || get_comments_number() ) :
						endy_comment_form();
					endif;
					?>

                </div><!-- .entry-content -->
                <aside class="col-sm-3 col-sm-offset-1">
                    <div id="post-sidebar-1" class="footer-sidebar widget-area" role="complementary">
		                <?php
		                if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-2' ) ):
		                endif;
		                ?>
                    </div>
<!--                    <h3>Tag's</h3>-->
<!--					--><?php //the_tags( '<p class="tags-list">', ' ', '</p>' ); ?>
                    <!--                                        <h3>Recent Posts</h3>-->
                    <ul class="blog-recent-post">
						<?php
						$args         = array(
							'numberposts' => '5',
							'post_status' => 'publish',
						);
						$recent_posts = wp_get_recent_posts( $args );
						foreach ( $recent_posts as $recent ) {
							$content = $recent["post_content"];
							$excerpt = wp_trim_words( $content, $num_words = 20, $more = null );
							$time    = date( 'd.m.Y', strtotime( $recent["post_date"] ) );

							echo '<li>'
							     . '<time class="entry-date">'
							     . $time
                                 .'</time>'
							     . '<h3><a href="'
							     . get_permalink( $recent["ID"] ) . '">'
							     . $recent["post_title"]
							     . '</a></h3>'
							     . '<p>' . $excerpt . '</p>'
							     . '</li> ';
						}
						wp_reset_query();
						?>
                    </ul>

                </aside>
            </div><!-- .row -->

            <div class="row">
                <footer class="entry-footer col-xs-12">
<!--					--><?php //endy_entry_footer(); ?>
                </footer><!-- .entry-footer --></div>
        </div><!-- .container-fluid -->
    </article><!-- #post-## -->
</div><!-- .row -->
