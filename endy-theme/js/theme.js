jQuery(document).ready(function ($) {
    'use strict';

    var $gallery = $('#portfolio-items');
    $gallery.lightGallery({
        selector: '.portfolio-item',

        // mode: 'lg-fade',
        // hash: true,
        // download: true,
        // enableDrag: true,
        // enableSwipe: true,
        thumbnail: true,
        animateThumb: true,
        showThumbByDefault: true
    });

    $gallery.imagesLoaded(
        function () {
            var $container = $gallery.imagesLoaded(function () {
                if ($.fn.isotope) {
                    var layoutMode = $('#portfolio-items').attr('data-layout');
                    $container.isotope({
                        itemSelector: '.item',
                        filter: '*',
                        resizesContainer: true,
                        layoutMode: layoutMode,
                        transitionDuration: '0.8s',
                        stagger: 30,
                        masonry: {
                            columnWidth: '.item'
                        }
                    });
                }
            });

        }
    );

// filter items on button click
    $('#filters').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        $('#portfolio-items').isotope({
            filter: filterValue
        });
        $gallery.data('lightGallery').destroy(true);
        $gallery.lightGallery({
            selector: '.portfolio-item' + filterValue
        });
        // console.log('.portfolio-item' + filterValue);
    });

    var $grid = $('.masonry-grid').imagesLoaded(function () {
        $grid.masonry({
            itemSelector: '.masonry-grid-item'
        });
    });
});
