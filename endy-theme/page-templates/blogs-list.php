<?php
/**
 * Template Name: Blogs list
 *
 * Description: Template for page with blogs list
 *
 * @package endy
 */

get_header(); ?>


    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <div class="row">
                <header class="col-sm-12">
                    <h1 class="page-title"><?php single_post_title(); ?></h1>
                    <title><?php echo get_post_meta( $post->ID, 'under_title', true ); ?></title>

                </header>
            </div>
            <br>
            <br>
            <br>

			<?php
			global $post;

			$the_query = new WP_Query( array( 'post_type' => 'post' ) );
			if ( $the_query->have_posts() ) : ?>

                <!-- pagination here -->


                <div class="row masonry-grid">
                    <!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post();

						get_template_part( 'template-parts/content', 'blog' );

					endwhile; ?>
                    <!-- end of the loop -->


                </div> <!-- .row -->

                <!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>


        </main><!-- #main -->
    </div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
