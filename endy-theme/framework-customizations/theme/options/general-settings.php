<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => __( 'General', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'general-box' => array(
				'title'   => __( 'General Settings', 'unyson' ),
				'type'    => 'box',
				'options' => array(
					'theme_style' => array(
						'label'   => __( 'Theme style', 'endy' ),
						'desc'    => __( 'Choose theme style', 'endy' ),
						'type'    => 'image-picker',
						'choices' => array(
							'minimal' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/01_Main_Minimal.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/01_Main_Minimal.jpg',
									'height' => 400
								),
							),
							'dress' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/02_Main_DressFood.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/02_Main_DressFood.jpg',
									'height' => 400
								),
							),
							'color' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/03_Main_Color.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/03_Main_Color.jpg',
									'height' => 400
								),
							),
							'rose' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/04_Main_Rose.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/04_Main_Rose.jpg',
									'height' => 400
								),
							),
							'photo' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/05_Main_Photographer.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/05_Main_Photographer.jpg',
									'height' => 400
								),
							),
							'dark' => array(
								'small' => get_template_directory_uri()
								           . '/assets/images/thumbs/11_About_Us_Dark.jpg',
								'large' => array(
									'src'    => get_template_directory_uri()
									            . '/assets/images/11_About_Us_Dark.jpg',
									'height' => 400
								),
							),
						),
					),
				),
			),
		),
	),
);