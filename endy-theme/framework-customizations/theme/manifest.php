<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$manifest                         = array();
$manifest['id']                   = 'endy';
$manifest['supported_extensions'] = array(
	'page-builder' => array(),
//	'slider'       => array(),
	'forms'        => array(),
//   'styling' => array(),
//   'breadcrumbs' => array(),
//   'events' => array(),
//	'feedback'     => array(),
//	'learning'     => array(),
//	'megamenu'     => array(),
//	'portfolio'    => array(),
//	'sidebars'     => array(),
);
