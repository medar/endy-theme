<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'custom_class' => array(
		'type'  => 'text',
		'label' => __( 'Custom class', 'endy' ),
		'desc'  => __( 'Add your custom classes here', 'endy' ),
	),
	'p_left'       => array(
		'type'  => 'text',
		'label' => __( 'Padding left', 'endy' ),
		'desc'  => __( 'Set padding from left in %', 'endy' ),
		'help'  => __('Disable on xs screen', 'endy'),
	),
	'p_right'      => array(
		'type'  => 'text',
		'label' => __( 'Padding right', 'endy' ),
		'desc'  => __( 'Set padding from right in %', 'endy' ),
		'help'  => __('Disable on xs screen', 'endy'),

	),
);