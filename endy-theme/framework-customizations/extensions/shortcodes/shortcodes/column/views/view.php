<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$class        = fw_ext_builder_get_item_width( 'page-builder', $atts['width'] . '/frontend_class' );
$custom_class = $atts['custom_class'];
$id           = 'column_id-' . fw_unique_increment();
$p_left       = $atts['p_left'];
$p_right      = $atts['p_right'];

echo '<style>';
echo '@media (min-width: 768px) {';
echo '#' . $id . ' {';
if ( strlen($p_left) ) {
	echo 'padding-left: ' . $p_left . '%' . ';';
}
if ( strlen($p_right) ) {
	echo 'padding-right: ' . $p_right . '%' . ';';
}
echo '}}';
echo '</style>';

?>
<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ) . ' ' . $custom_class; ?>">
	<?php echo do_shortcode( $content ); ?>
</div>
