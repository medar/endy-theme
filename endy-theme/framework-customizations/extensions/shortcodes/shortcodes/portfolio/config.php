<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Portfolio', 'unyson' ),
		'description' => __( 'Insert Portfolio', 'unyson' ),
		'tab'         => __( 'Content Elements', 'fw' ),
	)
);