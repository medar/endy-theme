<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'indentation' => array(
		'label'   => __( 'Indentaion', 'endy' ),
		'desc'    => __( 'Choose the style', 'endy' ),
		'type'    => 'select',
		'choices' => array(
			'add-margin' => __( 'With indent', 'endy' ),
			''           => __( 'Without indent', 'endy' )
		)
	),
	'columns'     => array(
		'label'   => __( 'Columns', 'endy' ),
		'desc'    => __( 'Choose number of columns', 'endy' ),
		'type'    => 'select',
		'choices' => array(
			'col_2' => __( 'Two columns', 'endy' ),
			'col_3' => __( 'Three columns', 'endy' )
		)
	),
	'layout_mode' => array(
		'value'   => 'masonry',
		'label'   => __( 'Layout mode', 'endy' ),
		'desc'    => __( 'Choose layout', 'endy' ),
		'type'    => 'select',
		'choices' => array(
			'masonry'           => __( 'Masonry style', 'endy' ),
			'packery'           => __( 'Packery style', 'endy' ),
			'fitRows'           => __( 'FitRows style', 'endy' ),
			'vertical'          => __( 'Vertical style', 'endy' ),
//			'cellsByRow'        => __( 'CellsByRow style', 'endy' ),
//			'masonryHorizontal' => __( 'MasonryHorizontal style', 'endy' ),
//			'fitColumns'        => __( 'FitColumns style', 'endy' ),
//			'cellsByColumn'     => __( 'CellsByColumn', 'endy' ),
//			'horiz'             => __( 'Horiz style', 'endy' ),
		)
	)
);