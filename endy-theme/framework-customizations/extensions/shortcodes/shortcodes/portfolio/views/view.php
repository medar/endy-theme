<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var $atts The shortcode attributes
 */

$layoutMode = $atts['layout_mode'];

global $post;

$terms = get_terms( "portfolio_tags" );
$count = count( $terms );
echo '<div id="filters" class="btn-group">';
echo '<button type="button" class="btn btn-default" data-filter="*">' . __( 'All', 'endy' ) . '</button>';
if ( $count > 0 ) {
	foreach ( $terms as $term ) {
		$termname = strtolower( $term->name );
		$termname = str_replace( ' ', '-', $termname );
		echo '<button type="button" class="btn btn-default" data-filter=".' . $termname . '">' . $term->name . '</button>';
	}
}
echo "</div>";
?>

<?php
// the query
$the_query = new WP_Query( array( 'post_type' => 'portfolio' ) ); ?>

<?php if ( $the_query->have_posts() ) : ?>

    <!-- pagination here -->

    <div class="row">
        <div id="portfolio-items" data-layout="<?php echo $layoutMode; ?>">
            <!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php
				$terms = get_the_terms( $post->ID, 'portfolio_tags' );

				if ( $terms && ! is_wp_error( $terms ) ) :
					$links = array();

					foreach ( $terms as $term ) {
						$links[] = $term->name;
					}
					$links = str_replace( ' ', '-', $links );
					$tax   = join( " ", $links );
				else :
					$tax = '';
				endif;
				?>

                <div class="item <?php
				echo $atts['indentation'] . ' ';
				echo $atts['columns'] . ' ';
				echo strtolower( $tax );
				?>">
                    <a class="portfolio-item  <?php echo strtolower( $tax ); ?>"
                       href="<?php echo get_the_post_thumbnail_url( null, 'full' ); ?>">
						<?php the_post_thumbnail(); ?>
                    </a>
                </div>
			<?php endwhile; ?>
            <!-- end of the loop -->

        </div> <!-- #portfolio-items -->

    </div> <!-- .row -->

    <!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
