<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Gap', 'unyson' ),
		'description' => __( 'Insert gap', 'unyson' ),
		'tab'         => __( 'Content Elements', 'fw' ),
		'icon' => 'dashicons dashicons-editor-break',
	)
);
