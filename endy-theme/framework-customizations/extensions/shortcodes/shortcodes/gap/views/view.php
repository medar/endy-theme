<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var $atts The shortcode attributes
 */

$height = $atts['height'];
$is_xs_hidden = $atts['is_xs_hidden'];
?>

<p class="<?php if($is_xs_hidden): echo 'hidden-xs'; endif; ?>" style="height: <?php echo $height . 'px'; ?>"></p>
