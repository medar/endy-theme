<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'height' => array(
		'label'   => __( 'Height', 'endy' ),
		'desc'    => __( 'Choose the height in pixels', 'endy' ),
		'type'    => 'text',
	),
	'is_xs_hidden' => array(
		'label' => __('Hidden in xs', 'endy'),
		'desc' => __('Is hidden on extra small view?', 'endy'),
		'type' => 'checkbox',
		'value' => false,
		'text' => __( 'True', 'endy')
	)
);