jQuery(document).ready(function ($) {
    'use strict';

    $('.owl-carousel').owlCarousel({
        nav: false,
        // loop:true,
        margin: 100,
        responsive: {
            0: {
                items: 2,
                margin: 50
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

});