<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var $atts The shortcode attributes
 */

$title = $atts['title'];
$desc  = $atts['desc'];
$number_of_posts = $atts['number_of_posts'];
?>
<div class="wrap-recent-post">
	<?php
	if ( $title ) {
		echo '<h2 class="rps-header">' . $title . '</h2>';
	}
	if ( $desc ) {
		echo '<title class="rps-desc">' . $desc . '</title>';
	}
	?>
    <br><br><br>
    <ul class="blog-recent-post owl-carousel owl-theme">
		<?php
		$args         = array(
			'numberposts' => $number_of_posts,
			'post_status' => 'publish',
		);
		$recent_posts = wp_get_recent_posts( $args );
		foreach ( $recent_posts as $recent ) {
			$content = $recent["post_content"];
			$excerpt = wp_trim_words( $content, $num_words = 20, $more = null );
			$time    = date( 'd.m.Y', strtotime( $recent["post_date"] ) );

			echo '<li class="owl-item">'
			     . '<time class="entry-date">'
			     . $time
			     . '</time>'
			     . '<h3><a href="'
			     . get_permalink( $recent["ID"] ) . '">'
			     . $recent["post_title"]
			     . '</a></h3>'
			     . '<p>' . $excerpt . '</p>'
			     . '</li> ';
		}
		wp_reset_query();
		?>
    </ul>
</div>