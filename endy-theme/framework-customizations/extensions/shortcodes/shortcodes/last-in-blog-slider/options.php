<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'title' => array(
		'label'   => __( 'Title', 'endy' ),
		'desc'    => __( 'Enter slider title', 'endy' ),
		'type'    => 'text',
	),
	'desc' => array(
		'label'   => __( 'Description', 'endy' ),
		'desc'    => __( 'Enter slider description', 'endy' ),
		'type'    => 'text',
	),
	'number_of_posts' => array(
		'default' => '10',
		'label'   => __( 'Posts', 'endy' ),
		'desc'    => __( 'Enter number of posts', 'endy' ),
		'type'    => 'text',
	),

);