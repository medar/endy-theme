<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Resent post slider', 'endy' ),
		'description' => __( 'Insert recent post slider', 'endy' ),
		'tab'         => __( 'Content Elements', 'fw' ),
		'icon' => 'dashicons dashicons-slides',
	)
);
