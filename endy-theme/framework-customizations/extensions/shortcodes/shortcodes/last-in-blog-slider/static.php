<?php
if (!defined('FW')){
	die('Forbidden');
}

// find the uri to the shortcode folder
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/last-in-blog-slider');
wp_enqueue_style(
	'css-owl-carousel-lib',
	$uri . '/static/assets/owl.carousel.min.css'
);
wp_enqueue_style(
	'css-owl-carousel-main',
	$uri . '/static/assets/owl.theme.endy.css'
);
wp_enqueue_script(
	'js-owl-carousel',
	$uri . '/static/owl.carousel.min.js',
	array('jquery'),
	'2.2.1',
	true
);
wp_enqueue_script(
	'js-owl-carousel-main',
	$uri . '/static/main.js',
	array('js-owl-carousel'),
	'1.0.0',
	true
);