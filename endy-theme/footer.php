<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package endy
 */

?>

</div><!-- #content -->
</div><!-- .row -->

<div class="row">
    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                    <div class="site-branding">
                            <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                                                     rel="home"><?php bloginfo( 'name' ); ?></a></p>
				            <?php

			            $description = get_bloginfo( 'description', 'display' );
			            if ( $description || is_customize_preview() ) : ?>
                            <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				            <?php
			            endif; ?>
                    </div><!-- .site-branding -->

                </div>
                <div class="col-sm-10">
                    <nav class="main-navigation toggled">
			            <?php wp_nav_menu( array(
				            'theme_location' => 'menu-2',
				            'menu_id'        => 'footer-menu'
			            ) ); ?>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="site-info">
                        &copy;
                        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'endy' ) ); ?>">
				            <?php printf( esc_html__( '%s Wordpress Theme', 'endy' ), 'Endy' ); ?>
                        </a>
                        <span>, <?php echo date("Y"); ?></span>
                        <br>
<!--                        <span class="sep"> | </span>-->
<!--			            --><?php //printf( esc_html__( 'Theme: %1$s by %2$s.', 'endy' ), 'endy',
//				            '<a href="https://automattic.com/" rel="designer">Max Medar</a>' ); ?>
<!--                        <br>-->
                        <span>All rights reserved</span>
                </div><!-- .site-info -->
                </div><!-- .col -->
                <div class="col-sm-8">


	                <?php if ( has_nav_menu( 'social' ) ) : // Check if there's a menu assigned to the 'social' location. ?>

                        <nav class="menu-social social-navigation menu" role="navigation" aria-label="<?php esc_attr_e( 'Social Menu', 'some' ); ?>">

			                <?php wp_nav_menu(
				                array(
					                'theme_location'  => 'social',
					                'container_class' => 'social-menu-wrapper',
					                'menu_id'         => 'menu-social-items',
					                'menu_class'      => 'menu-social-items',
					                'depth'           => 1,
					                'link_before'     => '<span class="screen-reader-text">',
					                'link_after'      => '</span>' . endy_get_svg( array( 'icon' => 'rating-full' ) ),
					                'fallback_cb'     => '',
				                )
			                ); ?>
                        </nav><!-- .menu-social -->

	                <?php endif; ?>

                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- col -->
    </footer><!-- #colophon -->
</div><!-- .row -->
</div><!-- .container -->

<?php wp_footer(); ?>

</div><!-- #page -->
</body>
</html>
