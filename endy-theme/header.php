<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package endy
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
    <a class="skip-link screen-reader-text"
       href="#content"><?php esc_html_e( 'Skip to content', 'endy' ); ?></a>
    <div class="container">
        <div class="row">

            <header id="masthead" class="site-header" role="banner">
                <div class="col-sm-2">
                    <div class="site-branding">
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                                                 rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
                            <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
							<?php
						endif; ?>
                    </div><!-- .site-branding -->
                </div><!-- .col-sm-2 -->

                <div class="col-sm-10">
                    <nav id="site-navigation" class="main-navigation" role="navigation">
                        <button class="menu-toggle pull-right hamburger hamburger--slider" aria-controls="primary-menu"
                                aria-expanded="false">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
						<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
                    </nav><!-- #site-navigation --></div>
            </header><!-- #masthead -->
        </div><!-- .row -->
        <div class="row">
            <div id="content" class="site-content col-sm-12">
