<?php

$portfolio = new CPT(array(
	'post_type_name' => 'portfolio',
	'singular' => __('Portfolio', 'endy'),
	'plural' => __('Portfolio', 'endy'),
	'slug' => 'portfolio'
),
	array(
		'supports' => array('title', 'editor', 'thumbnail', 'comments'),
		'menu_icon' => 'dashicons-portfolio'
	));

$portfolio->register_taxonomy(array(
	'taxonomy_name' => 'portfolio_tags',
	'singular' => __('Portfolio Tag', 'endy'),
	'plural' => __('Portfolio Tags', 'endy'),
	'slug' => 'portfolio-tag'
));